package com.example.cft_test_task.sync;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

public class AsyncTaskManager implements IProgressTracker, DialogInterface.OnCancelListener{

    private final OnTaskCompleteListener mTaskCompleteListener;
    private final ProgressDialog mProgressDialog;
    private Task mAsyncTask;
    private boolean service;

    public AsyncTaskManager(Context context, OnTaskCompleteListener taskCompleteListener, Boolean bService) {
        mTaskCompleteListener = taskCompleteListener;

        service = bService;

        mProgressDialog = new ProgressDialog(context);


        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(true);
        mProgressDialog.setOnCancelListener(this);


    }

    public void setupTask(Task asyncTask){
        // Keep task
        mAsyncTask = asyncTask;

        if (!service) {
            // Wire task to tracker
            mAsyncTask.setProgressTracker(this);
        }

        // Start task
        mAsyncTask.execute();
    }

    @Override
    public void onProgress(String message) {

        // Show dialog
        if (! mProgressDialog.isShowing() && !service){
            mProgressDialog.show();
        }

        if(!service) mProgressDialog.setMessage(message);
    }

    @Override
    public void onComplete() {
        // Close dialog
        mProgressDialog.dismiss();

        // Notify activity
        mTaskCompleteListener.onTaskComplete(mAsyncTask);

        // Reset task
        mAsyncTask = null;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        // Cancel task
        mAsyncTask.cancel(true);

        // Notify activity
        mTaskCompleteListener.onTaskComplete(mAsyncTask);

        // Reset task
        mAsyncTask = null;
    }
}

