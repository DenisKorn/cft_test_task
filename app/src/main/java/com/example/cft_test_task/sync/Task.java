package com.example.cft_test_task.sync;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.cft_test_task.BuildConfig;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Task extends AsyncTask<String, String, String> {

    public final static String COMMAND_NOT_NECESSARY = "";

    // Variables for http request
    //final String basicAuth  = "Basic " + Base64.encodeToString(BuildConfig.credentials.getBytes(), Base64.NO_WRAP);
    final String basicURL   = BuildConfig.basicCBR_URL;
    private HashMap<String, String> mMap = new HashMap<String, String>();
    private String mCommand = "";

    // Service variables
    protected final Resources mResources;
    private String mResult;
    private String mProgressMessage;
    private IProgressTracker mProgressTracker;
    private boolean isResultMessageNeeded;
    private Context context;

    public Task(Resources resources, String command, HashMap<String, String> map) {

        // Keep resources
        mResources = resources;
        mCommand = command;
        mMap = map;

        // Initialise initial pre-execute message
        mProgressMessage = "Запрос к серверу...";
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setProgressTracker(IProgressTracker progressTracker){

        // Attach tracker to process
        mProgressTracker = progressTracker;

        if (mProgressTracker != null){
            mProgressTracker.onProgress(mProgressMessage);

        }
    }

    @Override
    protected void onCancelled() {
        // detach tracker
        mProgressTracker = null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        // Upgrade progress message
        mProgressMessage = values[0];

        // send it to progress tracker
        if (mProgressTracker != null){
            mProgressTracker.onProgress(mProgressMessage);
        }
    }

    @Override
    protected void onPostExecute(String result) {

        // Update result
//            mResult = result;

        // Send result to progress tracker
        if (mProgressTracker != null){
            mProgressTracker.onComplete();
        }

        // Detach from progress tracker
        mProgressTracker = null;

    }

    @Override
    protected String doInBackground(String... params) {

        /*    Set<Map.Entry<String, String>> set = mMap.entrySet();

            Uri.Builder builder = Uri.parse(basicURL + mCommand).buildUpon();
            for (Map.Entry<String, String> item : set) {
                String value = item.getValue();
                builder.appendQueryParameter(item.getKey(), value);

            }

            String urlString = builder.build().toString();

            try {
                URL url = new URL(urlString);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setAllowUserInteraction(false);
                urlConnection.setDoOutput(true);
                OutputStream outputStream = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                for (Map.Entry<String, String> item : set) {
                    writer.write(item.getValue());
                }
                writer.flush();
                writer.close();


                try {
                    urlConnection.getResponseCode();
                } catch (Exception e) {
                    urlConnection.getResponseCode();
                }

                int statusCode = urlConnection.getResponseCode();

                if (statusCode == 200) {

                    InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                    if (inputStream == null) {
                        return "";
                    }
                    StringBuffer buffer = new StringBuffer();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line + "\n");
                    }

                    return buffer.toString();

                } else {
                    return "";
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }*/
        return sendGet();
    }

    public static String sendGet(){

      //  Set<Map.Entry<String, String>> set = mMap.entrySet();

        //Uri.Builder builder = Uri.parse(basicURL + mCommand).buildUpon();
        Uri.Builder builder = Uri.parse("https://www.cbr-xml-daily.ru/daily_json.js?=").buildUpon();
     /*   for (Map.Entry<String, String> item : set) {
            String value = item.getValue();
            builder.appendQueryParameter(item.getKey(), value);

        }*/

        String urlString = builder.build().toString();

        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setAllowUserInteraction(false);
            urlConnection.setDoOutput(true);
            OutputStream outputStream = urlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
         /*   for (Map.Entry<String, String> item : set) {
                writer.write(item.getValue());
            }*/
            writer.flush();
            writer.close();


            try {
                urlConnection.getResponseCode();
            } catch (Exception e) {
                urlConnection.getResponseCode();
            }

            int statusCode = urlConnection.getResponseCode();

            if (statusCode == 200) {

                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                if (inputStream == null) {
                    return "";
                }
                StringBuffer buffer = new StringBuffer();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }

                return buffer.toString();

            } else {
                return "";
            }

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }




}
