package com.example.cft_test_task.sync;

interface IProgressTracker {
    // Updates progress message
    void onProgress(String message);

    // Notifies about task completeness
    void onComplete();
}
