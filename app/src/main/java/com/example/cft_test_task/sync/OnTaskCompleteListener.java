package com.example.cft_test_task.sync;

public interface OnTaskCompleteListener {

    // Notifies about task completeness
    void onTaskComplete(Task task);
}
