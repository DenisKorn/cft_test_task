package com.example.cft_test_task;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {

    private ArrayList<HashMap<String, String>> hpCurrencies;
    private onClickListenerOwn calculateListener;

    public ItemAdapter(ArrayList<HashMap<String, String>> data, onClickListenerOwn listener){

        hpCurrencies = data;
        calculateListener = listener;

    }

    public void set(ArrayList<HashMap<String, String>> data){
        this.hpCurrencies = data;
    }

    public interface onClickListenerOwn{

        void onClick(String name, String value);

    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_res_view, parent, false);

        return new ItemViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position){

        HashMap<String, String> row = hpCurrencies.get(position);

        holder.tvName.setText(row.get("Name"));
        holder.tvValue.setText(row.get("Value"));
        holder.prefics = row.get("Prefics");

    }

    @Override
    public int getItemCount(){
        return hpCurrencies.size();
    }


    class ItemViewHolder extends RecyclerView.ViewHolder{

        private TextView tvName;
        private TextView tvValue;
        private LinearLayout ltEditVals;
        private String prefics = "";

        public ItemViewHolder(@NonNull View itemView){
            super(itemView);

            tvName = itemView.findViewById(R.id.tvValsName);
            tvValue = itemView.findViewById(R.id.tvValsValue);
            ltEditVals = itemView.findViewById(R.id.ltEditVals);

            ltEditVals.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    calculateListener.onClick(tvName.getText().toString(), tvValue.getText().toString());
                }
            });


        }


    }
}
