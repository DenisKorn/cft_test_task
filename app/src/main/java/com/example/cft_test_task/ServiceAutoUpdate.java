package com.example.cft_test_task;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;

import com.example.cft_test_task.jsnPack.JsnCurrency;
import com.example.cft_test_task.sync.AsyncTaskManager;
import com.example.cft_test_task.sync.OnTaskCompleteListener;
import com.example.cft_test_task.sync.Task;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.prefs.Preferences;

public class ServiceAutoUpdate extends Service  {
    public ServiceAutoUpdate() {
    }

    private boolean isWork = true;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
      //  throw new UnsupportedOperationException("Not yet implemented");
        return null;
    }

    @Override
    public void onCreate(){
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){

        runUpdate();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        isWork = false;
        System.out.println("1");
    }

    public void runUpdate(){

            new Thread(new Runnable() {
                public void run() {

                    while (isWork) {
                    String result = Task.sendGet();
                    writeResults(result);

                        System.out.println("0");

                        try {
                            TimeUnit.SECONDS.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                 //       stopSelf();
                    }
                }
            }).start();

    }

    public void writeResults(String resultIn){

        String result = "";

            result = resultIn;


        JsnCurrency jsnCurrency = new JsnCurrency(result);
        if(jsnCurrency.isValid()) {

            ArrayList<HashMap<String, String>> hm=jsnCurrency.data();

            SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor ed = sPref.edit();
            for (HashMap<String, String> hmEl : hm){

                String prefics = hmEl.get("Prefics");
                String name = hmEl.get("Name");
                String value = hmEl.get("Value");

                ed.putString(prefics +"_"+ name, value);

            }

            ed.apply();

        }
    }


}