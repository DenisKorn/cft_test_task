package com.example.cft_test_task.jsnPack;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class JsnCurrency extends JsnParser {

    private final static String RESULT_CODE_FAIL = "0000";
    private boolean isValid = false;

    private final ArrayList<HashMap<String, String>> data = new ArrayList<>();

    public JsnCurrency(String s){
        super(s);
    }

    public boolean isValid(){

        if (!this.getResultCode().equals(RESULT_CODE_FAIL)){
            getData();
            isValid = true;
        }

        return isValid;

    }

    private void getData(){

        JSONObject jsonObject = getJSON();

        try{

            JSONObject jsnValute = jsonObject.getJSONObject("Valute");

            ArrayList<String> keys = new ArrayList<>();
            String key;

            for(Iterator<String> it = jsnValute.keys(); it.hasNext();){
                key = it.next();
                keys.add(key);

                HashMap<String, String> hm = new HashMap<>();

                JSONObject jsnElemen = jsnValute.getJSONObject(key);

                hm.put("Prefics", key);
                hm.put("Name", jsnElemen.getString("Name"));
                hm.put("Value", jsnElemen.getString("Value"));

                data.add(hm);

            }

        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public ArrayList<HashMap<String,String>> data(){
        return data;
    }

}
