package com.example.cft_test_task.jsnPack;


import org.json.JSONException;
import org.json.JSONObject;

public class JsnParser {

    private String strJSON;
    private JSONObject jsnObj;

    public JsnParser(String s){
        strJSON = s;

        try {
            jsnObj = new JSONObject(strJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getResultCode(){

        if (jsnObj == null){
            return "0000";
        }
        try {
            String date = jsnObj.getString("Date");
            return date;
        } catch (JSONException e) {
            e.printStackTrace();
            return "0000";
        }
    }

    public String getMessage(){

        if (jsnObj == null){
            return "0000";
        }
        try {
            String resultMsg = jsnObj.getString("Message");
            return resultMsg;
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public JSONObject getJSON(){
        return jsnObj;
    }
}
