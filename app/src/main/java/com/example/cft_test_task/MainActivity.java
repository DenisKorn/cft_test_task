package com.example.cft_test_task;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cft_test_task.jsnPack.JsnCurrency;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import com.example.cft_test_task.sync.AsyncTaskManager;
import com.example.cft_test_task.sync.OnTaskCompleteListener;
import com.example.cft_test_task.sync.Task;



public class MainActivity extends AppCompatActivity implements OnTaskCompleteListener {

    public EditText tvRusValue;
    public TextView tvOtherCurrency;
    public TextView tvOtherCurrencyValue;
    public Button btRefresh;
    public RecyclerView rvListOfAllCurrencies;
    private LinearLayoutManager mManager;
    private double currentCurrencies;
    private double currentRusValue;
    private Switch autoUpdate;

    ArrayList<HashMap<String, String>> alAllCurrencies;

    private ItemAdapter itemAdapter;

    private AsyncTaskManager mAsyncTaskManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAsyncTaskManager = new AsyncTaskManager(this, this, false);

        setContentView(R.layout.activity_main);
        tvRusValue = findViewById(R.id.tvRusValue);
        tvOtherCurrency = findViewById(R.id.tvOtherVal);
        tvOtherCurrencyValue = findViewById(R.id.tvOtherValNumber);
        btRefresh = findViewById(R.id.btRefresh);
        rvListOfAllCurrencies = findViewById(R.id.rvAllCurrencies);
        autoUpdate = findViewById(R.id.autoUpdate);



        alAllCurrencies = new ArrayList<>();

        alAllCurrencies = readData();
        if(alAllCurrencies.isEmpty()){
            refreshBase();
        }

        mManager = new LinearLayoutManager(this);
        mManager.setReverseLayout(false);
       // mManager.setStackFromEnd(true);
        rvListOfAllCurrencies.setLayoutManager(mManager);

        itemAdapter = new ItemAdapter(alAllCurrencies, new ItemAdapter.onClickListenerOwn() {
            @Override
            public void onClick(String name, String value) {
                //получаем выбранную строку и ее значения
                tvOtherCurrency.setText(name);
                currentCurrencies = Double.parseDouble(value);
                String a = Double.toString(currentRusValue*currentCurrencies);
                tvOtherCurrencyValue.setText((a));

            }
        });

        rvListOfAllCurrencies.setAdapter(itemAdapter);

        final Timer[] timer = {new Timer()};
        final long DELAY = 1000; // in ms*/

        tvRusValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (timer[0] != null)
                    timer[0].cancel();

            }

            @Override
            public void afterTextChanged(Editable s) {
                //пересчитываем значение в другой валюте

                timer[0] = new Timer();

                timer[0].schedule(new TimerTask() {

                    @Override
                    public void run() {
                            InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(tvRusValue.getWindowToken(), 0);
                    }
                }, DELAY);

                         if (!s.toString().equals("")) {
                             currentRusValue = Double.parseDouble(s.toString());

                             String a = Double.toString(currentRusValue * currentCurrencies);

                             tvOtherCurrencyValue.setText(a);
                         }

            }
        });

        btRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                refreshBase();

            }
        });

        if (isServiceStarted(ServiceAutoUpdate.class)) autoUpdate.setChecked(true);

        autoUpdate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

              startKillStreams(b);

            }
        });

    }

    public void refreshBase(){

        HashMap<String, String> map = new HashMap<>();

        map.put(Task.COMMAND_NOT_NECESSARY, "");

        Task task = new Task(getResources(), "", map);
        mAsyncTaskManager.setupTask(task);

    };

    @Override
    public void onTaskComplete(Task task) {

        if (task.isCancelled()) {
            Toast.makeText(this, "Обновление прервано", Toast.LENGTH_SHORT).show();
            return;
        }

        String result = "";

        try {
            result = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        JsnCurrency jsnCurrency = new JsnCurrency(result);
        if(jsnCurrency.isValid()){

            writeData(jsnCurrency.data());
            alAllCurrencies = readData();
            itemAdapter.set(alAllCurrencies);
            itemAdapter.notifyDataSetChanged();

        }

    }

    public void writeData(ArrayList<HashMap<String, String>> hm){

        SharedPreferences sPref = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        for (HashMap<String, String> hmEl : hm){

            String prefics = hmEl.get("Prefics");
            String name = hmEl.get("Name");
            String value = hmEl.get("Value");

            ed.putString(prefics +"_"+ name, value);

        }

        ed.apply();

    }

    public ArrayList<HashMap<String, String>> readData(){

        ArrayList<HashMap<String, String>> arl = new ArrayList<>();

        Map<String, ?> hmALL = new HashMap<>();


        SharedPreferences sPref = getPreferences(MODE_PRIVATE);
        hmALL = sPref.getAll();

        for (Map.Entry<String, ?> entry : hmALL.entrySet()){

            HashMap<String, String> hmEach = new HashMap<>();
            String key = entry.getKey();
            String[] subString = key.split("_");
            String prefics = subString[0];
            String name = subString[1];
            String value = (String) entry.getValue();

            hmEach.put("Prefics", prefics);
            hmEach.put("Name", name);
            hmEach.put("Value", value);

            arl.add(hmEach);
        }

        return arl;
    }

    public void startKillStreams(boolean isChecked){

        if(isChecked) startServiceUpdate();
        else killService();

    }

    public void startServiceUpdate(){


        startService(new Intent(this, ServiceAutoUpdate.class));

    }


    public void killService(){

        stopService(new Intent(this, ServiceAutoUpdate.class));

    }

    public boolean isServiceStarted(Class<?> serviceClass){

        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
